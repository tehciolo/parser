class EventArgument {
  constructor (name) {
    this.name = name
    this.declaration = null
    this.type = null
    this.description = null
  }
}

module.exports.EventArgument = EventArgument
